import java.util.Hashtable;

import static org.junit.jupiter.api.Assertions.*;

public class MoneyExample {
    /*public void testMultiplication() {
        Money five = Money.dollar(5);
        assertEquals(Money.dollar(10), five.times(2));
        assertEquals(Money.dollar(15), five.times(3));
    }

    public void testFrancMultiplication() {
        Money five = Money.franc(5);
        assertEquals(Money.franc(10), five.times(2));
        assertEquals(Money.franc(15), five.times(3));
    }

    public void testEquality(){
        assertTrue(Money.dollar(5).equals(Money.dollar(5)));
        assertFalse(Money.dollar(5).equals(Money.dollar(6)));
        assertFalse(Money.franc(5).equals(Money.dollar(5)));
    }

    public void testCurrency() {
        assertEquals("USD", Money.dollar(1).currency());
        assertEquals("CHF", Money.franc(1).currency());
    }

    public void testDifferentClassEquality() {
        assertTrue(new Money(10, "CHF").equals(new Franc(10, "CHF")));
    }

    public void testSimpleAddition() {
        Money five= Money.dollar(5);
        Expression sum= five.plus(five);
        Bank bank= new Bank();
        Money reduced= bank.reduce(sum, "USD");
        assertEquals(Money.dollar(10), reduced);
    }

    public void testPlusReturnsSum() {
        Money five= Money.dollar(5);
        Expression result= five.plus(five);
        Sum sum= (Sum) result;
        assertEquals(five, sum.augend);
        assertEquals(five, sum.addend);
    }

    public void testReduceSum() {
        Expression sum= new Sum(Money.dollar(3), Money.dollar(4));
        Bank bank= new Bank();
        Money result= bank.reduce(sum, "USD");
        assertEquals(Money.dollar(7), result);
    }

    public void testReduceMoney() {
        Bank bank= new Bank();
        Money result= bank.reduce(Money.dollar(1), "USD");
        assertEquals(Money.dollar(1), result);
    }

    public void testReduceMoneyDifferentCurrency() {
        Bank bank= new Bank();
        bank.addRate("CHF", "USD", 2);
        Money result= bank.reduce(Money.franc(2), "USD");
        assertEquals(Money.dollar(1), result);
    }

    public void testArrayEquals() {
        assertEquals(new Object[] {"abc"}, new Object[] {"abc"});
    }

    public void testIdentityRate() {
        assertEquals(1, new Bank().rate("USD", "USD"));
    }

    public void testMixedAddition() {
        Expression fiveBucks= Money.dollar(5);
        Expression tenFrancs= Money.franc(10);
        Bank bank= new Bank();
        bank.addRate("CHF", "USD", 2);
        Money result= bank.reduce(fiveBucks.plus(tenFrancs), "USD");
        assertEquals(Money.dollar(10), result);
    }
    public void testSumPlusMoney() {
        Expression fiveBucks= Money.dollar(5);
        Expression tenFrancs= Money.franc(10);
        Bank bank= new Bank();
        bank.addRate("CHF", "USD", 2);
        Expression sum= new Sum(fiveBucks, tenFrancs).plus(fiveBucks);
        Money result= bank.reduce(sum, "USD");
        assertEquals(Money.dollar(15), result);
    }

    public void testSumTimes() {
        Expression fiveBucks= Money.dollar(5);
        Expression tenFrancs= Money.franc(10);
        Bank bank= new Bank();
        bank.addRate("CHF", "USD", 2);
        Expression sum= new Sum(fiveBucks, tenFrancs).times(2);
        Money result= bank.reduce(sum, "USD");
        assertEquals(Money.dollar(20), result);
    }

    public void testPlusSameCurrencyReturnsMoney() {
        Expression sum= Money.dollar(1).plus(Money.dollar(1));
        assertTrue(sum instanceof Money);
    }*/


    interface Expression{
        Money reduce(Bank bank, String to);
        Expression plus(Expression addend);
        Expression times(int multiplier);
    }

    class Money implements Expression{
        protected int amount;
        public Expression times(int multiplier) {
            return new Money(amount * multiplier, currency);
        }
        protected String currency;
        String currency() {
            return currency;
        }
        public boolean equals(Object object) {
            Money money = (Money) object;
            return amount == money.amount
                    && currency().equals(money.currency());
        }

       /* static Money dollar(int amount) {
            return new Dollar(amount, "USD");
        }

        static Money franc(int amount) {
            return new Franc(amount, "CHF");
        }*/

        Money(int amount, String currency) {
            this.amount = amount;
            this.currency = currency;
        }

        public String toString() {
            return amount + " " + currency;
        }
        public Money reduce(Bank bank, String to) {
            int rate = bank.rate(currency, to);
            return new Money(amount / rate, to);
        }

        public Expression plus(Expression addend) {
            return new Sum(this, addend);
        }

    }
    /*class Dollar extends Money{
        private String currency;
        Dollar(int amount, String currency) {
            super(amount, currency);
        }
        String currency() {
            return currency;
        }

        public Money times(int multiplier) {
            return new Money(amount * multiplier, currency);
        }

        public boolean equals(Object object){
            Money money= (Money) object;
            return amount == money.amount;
        }

       // private int amount;
    }

    class Franc extends Money {
        //private int amount;
        private String currency;
        Franc(int amount, String currency) {
            super(amount, currency);
        }
        String currency() {
            return currency;
        }
        public Money times(int multiplier) {
            return new Money(amount * multiplier, currency);
        }

        public boolean equals(Object object) {
            Money money= (Money) object;
            return amount == money.amount;
        }
    }*/

    class Bank{
        private Hashtable rates= new Hashtable();
        Money reduce(Expression source, String to) {
            return source.reduce(this, to);
        }

        int rate(String from, String to) {
            if (from.equals(to)) return 1;
            Integer rate= (Integer) rates.get(new Pair(from, to));
            return rate.intValue();
        }
        void addRate(String from, String to, int rate) {
            rates.put(new Pair(from, to), Integer.valueOf(rate));//new version adjustment old:new integer(rate)
        }
    }

    class Sum implements Expression{
        Expression augend;
        Expression addend;
        Sum(Expression augend, Expression addend) {
            this.augend= augend;
            this.addend= addend;
        }
        public Money reduce(Bank bank, String to) {
            int amount= augend.reduce(bank, to).amount
                    + addend.reduce(bank, to).amount;
            return new Money(amount, to);
        }
        public Expression plus(Expression addend) {
            return new Sum(this, addend);
        }
        public Expression times(int multiplier) {
            return new Sum(augend.times(multiplier),addend.times(multiplier));
        }
    }

    private class Pair {
        private String from;
        private String to;
        Pair(String from, String to) {
            this.from= from;
            this.to= to;
        }
        public boolean equals(Object object) {
            Pair pair= (Pair) object;
            return from.equals(pair.from) && to.equals(pair.to);
        }

        public int hashCode() {
            return 0;
        }
    }
}


